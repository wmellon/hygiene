(function() {
  'use strict';

  angular.
  module('viewUser').
  config(angularConfig);

  angularConfig.$inject = ['$locationProvider', '$routeProvider'];
  // Angular Routing when route /user is present.
  function angularConfig($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');
    // Route for template render.
    $routeProvider.when('/user', {
      template: '<view-user></view-user>'
    });
  }
})();