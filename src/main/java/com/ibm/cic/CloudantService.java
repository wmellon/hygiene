package com.ibm.cic;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cloudant.client.api.ClientBuilder;
import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import com.cloudant.client.api.model.Response;

@Service
public class CloudantService {

	private Database database;

	/**
	 * The username, password, and dbName are environment variables which need
	 * to be set in the server.env file.  The CloudantClient allows access to the
	 * Cloudant API and ClientBuilder allows us to bootstrap to the API.
	 * The Database class contains the API implementation of the Cloudant database.
	 * We access the reference to the database by setting the database to the client, and give
	 * the database name.  We set it to false flag indicating whether to create the database 
	 * if it doesn't exist.  Read the javadoc on this, because it will return an instance,
	 * but the first time this is used will throw an exception.
	 * 
	 * Replace this information with your Cloudant Resource:
	 * CLOUDANT_USERNAME=username
	 * CLOUDANT_PASSWORD=password
	 * CLOUDANT_DATABASE=dbname
	 */
	public CloudantService() {
//		String username = System.getenv("CLOUDANT_USERNAME");
//		String password = System.getenv("CLOUDANT_PASSWORD");
//		String dbName = System.getenv("CLOUDANT_DATABASE_NAME");
		
		
		CloudantClient client = ClientBuilder.account("525edc51-711c-4ece-ac29-99808c95bb5a-bluemix")
				.username("525edc51-711c-4ece-ac29-99808c95bb5a-bluemix")
				.password("fa30af0c07e22308aa1b4bebf350be7c073b8e6d7099170c43c61210c2cc16ae").build();
		database = client.database("users", false);
	}
	
	/**
	 * Returns a document with a specified id from the database, and deserializes it back into
	 * a Java Object of the return type.
	 * 
	 * @param id - int
	 * @return User
	 */
	public User findDocById(String id) {
		return database.find(User.class, id);
	}

	/**
	 * Returns the documents in the database as a List.  This creates a new ArrayList<>
	 * of type List<User>, and we use a try/catch block to set the new list to the result
	 * of the documents request as long as the database exists.  The 'getDocsAs(...)' 
	 * deserializes the content it's returning to a List of the specified type, which in here
	 * is the type User.
	 * 
	 * @return List<User>
	 */
	public List<User> getAllUsers() {
		List<User> results = new ArrayList<User>();

		try {
			results = database.getAllDocsRequestBuilder().includeDocs(true).build().getResponse().getDocsAs(User.class);
		} catch (IOException e) {

			e.printStackTrace();
		}
		return results;
	}

	/**
	 * Returns a boolean if the User was deleted.  A Response is created
	 * because they typically need an _id and _rev values, which are returned
	 * from the db.  This works because when we use the remove(user) function,
	 * the _id and_rev MUST be correct to the object we want to remove.
	 * 
	 * @param user - User
	 * @return boolean
	 */
	public boolean delete(User user) {
		Response response = database.remove(user);
		return response.getError() != null;
	}

	/**
	 * Returns a boolean if the User was added to the db.  A Response
	 * is created because the _id and _rev are needed.  This checks to make
	 * sure these values are the same with the Object, and will post to the
	 * database if they are.
	 * 
	 * @param user - User
	 * @return boolean
	 */
	public boolean post(User user) {
		Response response = database.post(user);
		
		// we set the _id and _rev of the User object by using the Response object to return them
		user.set_id(response.getId());
		user.set_rev(response.getRev());
		
		return response.getError() != null;
	}

	/**
	 * Returns a boolean if the User was updated.  A Response object is
	 * created to check if the User object has the correct _id and _rev
	 * to be updated.  
	 * 
	 * @param user - User
	 * @return boolean
	 */
	public boolean put(User user) {
		Response response = database.update(user);
		return response.getError() != null;
	}

}
