(function() {
  'use strict';
  //A light-weight model layer that bridges the gap between AngularJS and your RESTful APIs.
  //Look up Angular Module Factory for more info.
  angular.
  module('viewUser').
  factory('userService', userService);

  //Inject http object into user service.
  userService.$inject = ['$http'];

  function userService($http) {
    var service = {
      createUser,
      getUsers,
      deleteUser,
      updateUser
    }
    return service;
    //Must have a callback.
    //User is already a json.
    function createUser(user, cb) {
      $http({
        method: 'POST',
        data: user,
        url: 'user'
      }).then(function successCallback(response) {
    	  
    	  //console.log(user);
    	  //console.log("test");
    	  //console.log("SuccessCallback response:"+response);
    	  
        cb(response);
        
      }, function errorCallback(response) {
    	  
    	  //console.log(user);
    	  //console.log("errorcallback"+response);
    	  
        cb(response);
        
      });
    }

    // update user
    function updateUser(user, userID, cb) {
      $http({
        method: 'PUT',
        data: user,
        url: 'user/' + userID
      }).then(function successCallback(response) {
        cb(true);
      }, function errorCallback(response) {
        cb(false);
      });
    }

    // gets users from cloudant - JSON
    function getUsers(cb) {
      $http({
        method: 'GET',
        url: 'user'
      }).then(function successCallback(response) {
        cb(response);
      }, function errorCallback(response) {
        cb(false);
      });
    }

    // send a user delete request
    function deleteUser(userID, cb) {
      $http({
        method: 'DELETE',
        url: 'user/' + userID
      }).then(function successCallback(response) {
        cb(true);
      }, function errorCallback(response) {
        cb(false);
      });
    }
  }
})();