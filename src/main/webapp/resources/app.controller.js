(function() {
  'use strict';

  // Controller the MainApp Module.
  // Application controller similar to rails in some aspects.
  angular.module('mainApp')
    .controller('mainAppController', MainAppController);

  function MainAppController() {
    var self = this;

    self.titleName = "One Messed Up Site";
  }

})();